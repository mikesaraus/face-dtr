export default function () {
  return {
    devices: {
      video: [],
      audio: [],
      other: []
    },
    camera: null,
    selectedFaceOption: 'TinyFaceDetectorOptions'
  }
}
