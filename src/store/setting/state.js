export default function () {
  return {
    minFaceMatch: 0.6,
    maxTestImages: 5,
    predictionInterval: 10
  }
}
